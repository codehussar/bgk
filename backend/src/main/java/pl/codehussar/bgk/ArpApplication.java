package pl.codehussar.bgk;

import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

@Slf4j
@SpringBootApplication
@PropertySource(ignoreResourceNotFound = true, value = "file:${catalina.home}/conf/bgk.properties")
public class ArpApplication extends SpringBootServletInitializer {

  public ArpApplication() {
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    log.info("");
    return configureBuilder(builder);
  }

  public static void main(String[] args) {
    configureBuilder(new SpringApplicationBuilder()).run(args);
  }

  private static SpringApplicationBuilder configureBuilder(SpringApplicationBuilder builder) {
    return builder.sources(ArpApplication.class)
        .properties(Collections.singletonMap("spring.config.name", "bgk"));
  }
}
