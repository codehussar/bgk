package pl.codehussar.bgk.model;

public class GetBossAssesmentsRequest {

  String boss;

  public GetBossAssesmentsRequest() {
  }

  public String getBoss() {
    return boss;
  }

  public void setBoss(String boss) {
    this.boss = boss;
  }

  public GetBossAssesmentsRequest(String boss) {
    this.boss = boss;
  }
}
