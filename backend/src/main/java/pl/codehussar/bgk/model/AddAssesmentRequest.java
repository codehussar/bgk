package pl.codehussar.bgk.model;

public class AddAssesmentRequest {

  String userFrom;

  String userTo;

  String data;

  public AddAssesmentRequest(String userTo, String userFrom, String data) {
    this.userTo = userTo;
    this.userFrom = userFrom;
    this.data = data;
  }

  public AddAssesmentRequest() {
  }

  public String getUserFrom() {
    return userFrom;
  }

  public void setUserFrom(String userFrom) {
    this.userFrom = userFrom;
  }

  public String getUserTo() {
    return userTo;
  }

  public void setUserTo(String userTo) {
    this.userTo = userTo;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

}
