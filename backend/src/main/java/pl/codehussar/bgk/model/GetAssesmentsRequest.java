package pl.codehussar.bgk.model;

public class GetAssesmentsRequest {

  String user;

  public GetAssesmentsRequest() {
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public GetAssesmentsRequest(String user) {
    this.user = user;
  }
}
