package pl.codehussar.bgk.api;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.codehussar.bgk.model.AddAssesmentRequest;
import pl.codehussar.bgk.model.GetAssesmentsRequest;
import pl.codehussar.bgk.model.GetBossAssesmentsRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class Controller {

  private List<AddAssesmentRequest> assesments = new ArrayList<>();


  @PostMapping(value = "/addAssesment")
  @ResponseBody
  public void addAssesment(@RequestBody AddAssesmentRequest addAssesmentRequest) {
    assesments.add(addAssesmentRequest);
  }

  @GetMapping("/getAssesmentsByUserTo")
  @ResponseBody
  public String getAssesmentsByUserTo(
      @RequestBody GetAssesmentsRequest getAssesmentsRequest) {
      List<AddAssesmentRequest> output = new ArrayList<>();
      for(AddAssesmentRequest temp: assesments){
        if(temp.getUserTo().equals(getAssesmentsRequest.getUser())){
          output.add(temp);
        }
      }
    return new Gson().toJson(output);
  }

  @GetMapping("/getAssesmentsByUserFrom")
  @ResponseBody
  public String getAssesmentsByUserFrom(
      @RequestBody GetAssesmentsRequest getAssesmentsRequest) {
    List<AddAssesmentRequest> output = new ArrayList<>();
    for(AddAssesmentRequest temp: assesments){
      if(temp.getUserFrom().equals(getAssesmentsRequest.getUser())){
        output.add(temp);
      }
    }
    return new Gson().toJson(output);
  }

  @GetMapping("/getBossAssesments")
  public String getBossAssesments(@RequestBody GetBossAssesmentsRequest getBossAssesmentsRequest) {
    return new Gson().toJson(assesments);
  }

}

