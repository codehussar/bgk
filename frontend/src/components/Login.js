import React, { useState } from 'react';

// import TextField from '@material-ui/core/TextField';

import styled from 'styled-components'

import { Button, Input } from 'semantic-ui-react'


export function Login(props) {

    // const { classes } = this.props;

    const [login, setName] = useState("Jan Kowalski");
    const [password, setPassword] = useState("demo1");

    const [error, setError] = useState(0)

    let handleName = event => {
        setName(event.target.value);

    }
    let handlePassowrd = event => {
        setPassword(event.target.value);

    }
    let awalibleAccounts = ['jan.kowalski@gmail.com']
    let handleLogin = e => {
        props.auth(login, password)
        if (awalibleAccounts.includes(login)) {
        }
        else {
            setError(1)
        }
    }
    const handleDown = e => {
        if (e.key === 'Enter') {
            handleLogin();
        }
    }

    return <Centred>
        <CardCentered>
            <MyImage src="./resources/logo.png" alt="logo" height="" />
            <br></br>
            <MyDiv>
                {error ? <Error type={error}></Error> : null}
                <Input
                    id="outlined-name"
                    label="Login"
                    value={login}
                    onChange={handleName}
                    onKeyDown={handleDown}
                    className="labels"
                />
                <br/><br/>
                <Input
                    id="outlined-password-input"
                    label="Passowrd"
                    type="password"
                    value={password}
                    onChange={handlePassowrd}
                    onKeyDown={handleDown}
                    autoComplete="current-password"
                    className="labels"
                />
                <br></br><br />
                <Button color="blue" onClick={handleLogin} >
                    Login
                </Button>
            </MyDiv>
        </CardCentered>
    </Centred>
}

function Error() {

    return (
        <p style={{ color: "#F00", marginBottom: "10px" }}>Błąd - podano nieprawidłowy adres e-mail </p>
    )
}

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-color:#CD1D34
`

const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;
text-align: center;
`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
border-radius: 10px;
display: inline-block;
`


const MyImage = styled.img`
height:137px;
margin-bottom: 10vh;
`


// ./resources/ 

