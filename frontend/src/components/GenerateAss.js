import React, { useState } from "react";

import styled from 'styled-components'

import { Grid, Rating, Divider } from 'semantic-ui-react'


function GenerateAss(props) {

    const checktag = (num) => {
        switch (num) {
            case 0:
                return 'Professinalism'
            case 1:
                return 'Partnership'
            case 2:
                return 'Engagment'
            default:
                return null
        }

    }
    const listOfAsses = props.list.map((raw) => {
        console.log('raw')
        console.log(raw)
        let obj = JSON.parse(raw.data)
        console.log(obj)

        return (
            <React.Fragment key={obj.stars + obj.name + obj.to + Math.random()}>
                <Grid.Row style={{ backgroundColor: '#DA2038', borderRadius: '30px' }}  >
                    <Grid.Column style={{ margin: 'auto', padding: 0 }} width={3}>
                        <Profile src={`./resources/face${obj.profile}.png`} /><br />
                        {obj.from}
                        {/* <Image src='/images/wireframe/image.png' /> */}
                    </Grid.Column>
                    <Grid.Column width={13} style={{ margin: 'auto', textAlign: 'center' }}>
                        {checktag(obj.tag)}<br />
                        <Rating icon='star' disabled={true} defaultRating={3} maxRating={5} /><br />
                        {obj.message}
                        <br />
                        <br />
                        {props.boss ? `Do: ${obj.to}` : null}
                        {/* <Image src='/images/wireframe/centered-paragraph.png' /> */}
                    </Grid.Column>
                </Grid.Row >
                <Divider horizontal>&nbsp;</Divider>

            </React.Fragment>
        )
    });

    return (
        <Grid className="special_grid" >
            {listOfAsses}
        </Grid>
    )
}
export { GenerateAss }

const Profile = styled.img`
border-radius: 50%;
width: 100%;
margin: 0;
padding: 0;
`


