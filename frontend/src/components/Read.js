import React, { useState } from 'react'

import styled from 'styled-components'
import { Button } from 'semantic-ui-react'

import { GenerateAss } from './GenerateAss'
import axios from 'axios';

function Read(props) {
    let [data, setData] = useState(0)
    let [flag, setFlag] = useState(false)
    if (!flag) {
        axios.post('http://uranus.wat.edu.pl:7050/bgk/getAssesmentsByUserFrom', {

            user: props.login,
        })
            .then(function (response) {
                // console.log(response);
                setData(response.data);
                setFlag(true);
            })
            .catch(function (error) {
                console.log(error);
            })
            .finally(function () {
                // always executed
            });
    }

    return (
        <Centred>
            <CardCentered>
                <MyDiv>
                    <MyImage src="./resources/logo.png" alt="logo" height="" /><br />
                    {flag ? <GenerateAss boss={true} list={data} /> : null}
                    <br />
                    <Button className='Big big2' onClick={() => { props.close(0) }} positive>Back</Button>
                </MyDiv>
            </CardCentered>
        </Centred >
    )

}
export { Read }




const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-color:#CD1D34

`// background-color:#1C1C24FF
const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;
background-color:#CD1D34

`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
border-radius: 10px;
// background-color: #e6e6e6;
background-color:#CD1D34

display: inline-block;
color: white;
`




const MyImage = styled.img`
height:137px;
margin-bottom: 10vh;
`