import React, { useState } from 'react'

import styled from 'styled-components'
import { Button } from 'semantic-ui-react'


function Thanks(params) {

    return (
        <Centred>
            <CardCentered>
                <MyDiv>
                    <MyImage src="./resources/logo.png" alt="logo" height="" /><br />
                    Thenk you for your assessment 
                    <br />
                    <br />
                    <Button className='Big big2' onClick={() => { params.close(0) }} positive>Go back</Button>
                </MyDiv>
            </CardCentered>
        </Centred >
    )

}
export { Thanks }



const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-color:#CD1D34

`// background-color:#1C1C24FF
const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;
`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
border-radius: 10px;
// background-color: #e6e6e6;
display: inline-block;
color: white;
`




const MyImage = styled.img`
height:137px;
margin-bottom: 10vh;
`