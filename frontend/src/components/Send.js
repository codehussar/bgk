import React, { useState } from 'react'

import styled from 'styled-components'
import axios from 'axios';
import { Button, Rating, Form, TextArea, Input } from 'semantic-ui-react'

import { Thanks } from "./Thanks";

function Send(props) {

    const [thanks, setThanks] = useState(0)
    const [message, setMess] = useState('')
    const [target, setTarget] = useState('')
    const [buttonTable, setButtons] = useState(['blue', 'blue', 'blue'])
    let starsReating = 3;

    const handleRate = (e) => {
        starsReating = e.target.getAttribute('aria-posinset');
    }
    const handleChange = (e) => {
        setMess(e.target.value)
    }
    const handleTags = (num) => {
        let tab = ['blue', 'blue', 'blue'];
        tab[num] = 'green'
        setButtons(tab)
    }

    const handleSend = async () => {
        let obj = {};
        obj.from = props.login;
        obj.to = target;
        obj.stars = starsReating-1;
        obj.message = message;
        obj.profile = Math.floor(Math.random() * 7) + 1;
        obj.tag = buttonTable.indexOf("green")
        let jsonstr = await JSON.stringify(obj)
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.post('http://uranus.wat.edu.pl:7050/bgk/addAssesment', {
            userFrom: props.login,
            userTo: target,
            data: jsonstr

        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            })
            .finally(function () {
                // always executed
            });
        setThanks(1)
    }

    if (thanks) {
        return <Thanks close={props.close} login={props.login} />
    }

    return (
        <Centred>
            <CardCentered>
                <MyDiv>
                    <MyImage src="./resources/logo.png" alt="logo" height="" /><br />
                    <div>
                        <Rating icon='star' size='massive' defaultRating={3} maxRating={5} onRate={handleRate} />
                        <br />
                        <br />
                        <Input
                            id="outlined-name"
                            label="Addressee"
                            value={target}
                            onChange={(e) => { setTarget(e.target.value) }}
                            className="labels"
                            placeholder={props.login}
                        />
                        <br />
                        <br />
                        <Button.Group size="mini" style={{position:'relative',marginLeft:"-5px"}}>
                            <Button color={buttonTable[0]} onClick={() => { handleTags(0) }} >Professinalism</Button>
                            <Button.Or  text="" />
                            <Button color={buttonTable[1]} onClick={() => { handleTags(1) }} >Partnership</Button>
                            <Button.Or text="" />
                            <Button color={buttonTable[2]} onClick={() => { handleTags(2) }}>Engagment</Button>
                        </Button.Group>
                        <br />
                        <br />
                        <Form>
                            <TextArea placeholder='Napisz którki opis...' value={message} onChange={handleChange} />
                        </Form>
                        <br />
                    </div>
                    <Button.Group >
                        <Button className='Big' color='yellow' onClick={() => { props.close(0) }}>Back to Menu</Button>
                        <Button.Or className='BigMiddle' text='lub' />
                        <Button className='Big big2' positive onClick={() => { handleSend() }}>Send</Button>
                    </Button.Group>
                </MyDiv>
            </CardCentered>
        </Centred >
    )

}
export { Send }



const Centred = styled.div`
height:100vh;
    width: 100vw;
    text-align: center;
 background-color:#CD1D34
`

const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;
background-color: #CD1D34;


`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
// width: 80vw;

border-radius: 10px;
background-color: #CD1D34;
display: inline-block;
color: white;
`



const MyImage = styled.img`
height:137px;
margin-bottom: 10vh;
`