import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

import { Login } from './components/Login'
import { Send } from './components/Send'
import { Show } from './components/Show'
import { Read } from './components/Read'


import styled from 'styled-components'

import { Button } from 'semantic-ui-react'


function App() {

  const [login, setLogin] = useState(0)
  const [send, setSend] = useState(0)
  const [show, setShow] = useState(0)
  const [read, setRead] = useState(0)
  let boss = 0;
  if (login === 0) {
    return (
      <Login auth={setLogin} />
    )
  }

  if (login === "Jan Kowalski") {
    boss = 1
  }

  if (send === 1) {
    return (
      <Send close={setSend} login={login} />
    )
  }
  if (show === 1) {
    return (
      <Show close={setShow} login={login} />
    )
  }

  // Read Someone
  if (read === 1) {
    return (
      <Read close={setRead} login={login} />
    )
  }

  return (
    <Centred>
      <CardCentered>
        <MyDiv>
          <MyImage src="./resources/logo.png" alt="logo" height="" /><br />
          <Button.Group >
            <Button className='Big' color='blue' onClick={() => { setShow(1) }}>Show assessments</Button>
            <Button.Or className='BigMiddle' text='lub' />
            <Button className='Big big2' positive onClick={() => { setSend(1) }}>Send assessments</Button>
          </Button.Group><br /><br />
          {boss ? <Button color="olive" className='Big big2' onClick={() => { setRead(1) }}>Employies assessments</Button> : null}
        </MyDiv>
      </CardCentered>
    </Centred >
  );
}

export default App;

const Centred = styled.div`
    height: 100vh;
    width: 100vw;
    text-align: center;
    background-color:#CD1D34

`// background-color:#1C1C24FF
const CardCentered = styled.div`
display:absolute;
z-index:2
padding-top:10vh;
`

const MyDiv = styled.div`
text-align: center;
padding: 20px;
border-radius: 10px;
// background-color: #e6e6e6;
display: inline-block;
color: white;
`



const MyImage = styled.img`
height:137px;
margin-bottom: 10vh;
`